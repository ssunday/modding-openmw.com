/*
  momw - kbd-nav.js
  Copyright (C) 2020  Hristos N. Triantafillou

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var listNumDiv = document.getElementById("listnum");
var modListDiv = document.getElementById("modlist");
var nextDiv = document.getElementById("next");
var prevDiv = document.getElementById("prev");

function listCtl() {
    document.addEventListener("keydown", (event) => {
        const keyName = event.key;

        if (keyName === "Control")
            return;

        if (event.ctrlKey) {
            var listNum = listNumDiv.getAttribute("data-listnum");
            var modList = modListDiv.getAttribute("data-modlist");
            var next = nextDiv.getAttribute("data-next");
            var prev = prevDiv.getAttribute("data-prev");

            if (keyName === "ArrowLeft") {
                if (prev !== "False") {
                    var prevNum = parseInt(listNum) - 1;
                    var url = `/lists/${modList}/${prevNum}/`;
                    document.location.href = url;
                }
            }

            if (keyName === "ArrowRight") {
                if (next !== "False") {
                    var nextNum = parseInt(listNum) + 1;
                    var url = `/lists/${modList}/${nextNum}/`;
                    document.location.href = url;
                }
            }
        }
    }, false);
}

window.onload = listCtl;
