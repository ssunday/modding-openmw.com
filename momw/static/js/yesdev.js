/*
  momw - yesdev.js
  Copyright (C) 2024 MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const storageKeyYesDev = "yesdev";

function SetupYesDev() {
    const modDetailLinks = document.getElementsByClassName("mod-detail-link");
    const listPresetBtns = document.getElementsByClassName("modlist-preset");
    const cfgValSpan = document.getElementById("yesDev");
    const currentYesDev = localStorage.getItem(storageKeyYesDev) === "true";
    const yesdev = document.getElementById("yesdev");
    const yesdevHidden = document.getElementById("yesdev-hidden");

    if (yesdev) {
        yesdev.addEventListener("click", function() {
            localStorage.setItem(storageKeyYesDev, yesdev.checked.toString());
        });
        yesdev.checked = currentYesDev;
    }

    if (yesdevHidden)
        if (!currentYesDev)
            yesdevHidden.disabled = true;

    if (cfgValSpan)
        if (currentYesDev) {
            cfgValSpan.textContent = "Yes";
        } else {
            cfgValSpan.textContent = "No";
        }

    if (listPresetBtns)
        if (currentYesDev)
            for (const btn of listPresetBtns)
                btn.href += "?dev=on";

    if ((modDetailLinks) && (currentYesDev))
        for (const lnk of modDetailLinks)
            lnk.href += "?dev=on";
}

SetupYesDev();
