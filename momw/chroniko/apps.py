from django.apps import AppConfig


class ChronikoConfig(AppConfig):
    name = "chroniko"
    verbose_name = "Chroniko Blogging Engine"
