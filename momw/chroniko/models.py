import pytz

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.db import models
from django.utils.translation import gettext_lazy as _
from taggit.managers import TaggableManager
from taggit.models import TagBase, GenericTaggedItemBase
from utilz.managers import (
    DraftThingManager,
    EnabledTagManager,
    HiddenThingManager,
    LiveThingManager,
)


TZ = pytz.timezone(settings.TIME_ZONE)
try:
    SHORT_URLS = settings.BLOG_SHORT_URL
except AttributeError:
    SHORT_URLS = False


class BlogCategory(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(
        help_text="Suggested value automatically generated from title.", unique=True
    )
    description = models.TextField()

    class Meta:
        db_table = "blog_categories"
        ordering = ["title"]
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:category_detail", kwargs={"slug": self.slug})


class BlogEntry(models.Model):
    LIVE = 1
    DRAFT = 2
    HIDDEN = 3
    STATUS_CHOICES = ((LIVE, "Live"), (DRAFT, "Draft"), (HIDDEN, "Hidden"))
    objects = models.Manager()
    live = LiveThingManager()
    draft = DraftThingManager()
    hidden = HiddenThingManager()
    body = models.TextField()
    excerpt = models.TextField(blank=True)
    featured = models.BooleanField(default=False)
    date_added = models.DateTimeField()
    date_updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=DRAFT)
    title = models.CharField(max_length=250)
    author = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    category = models.ForeignKey(BlogCategory, on_delete=models.PROTECT)
    tags = TaggableManager(through="TaggedEntry", blank=True)

    class Meta:
        db_table = "blog_entries"
        ordering = ["-date_added"]
        verbose_name = _("Entry")
        verbose_name_plural = _("Entries")

    def __str__(self):
        return self.title

    def get_absolute_url(self, short=SHORT_URLS):
        if short:
            url = reverse("slug_root", kwargs={"slug": self.slug})
        else:
            url = reverse(
                "blog:entry_detail",
                kwargs={
                    "year": self.date_added.astimezone(TZ).strftime("%Y"),
                    "month": self.date_added.astimezone(TZ).strftime("%m"),
                    "day": self.date_added.astimezone(TZ).strftime("%d"),
                    "slug": self.slug,
                },
            )
        return url

    def get_status_string(self):
        return self.STATUS_CHOICES[self.status - 1][1]


class BlogTag(TagBase):
    objects = models.Manager()
    enabled = EnabledTagManager()
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = "blog_tags"
        ordering = ["-name"]
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    @property
    def tagged_entries(self):
        return BlogEntry.live.filter(tags__pk=self.pk)

    @property
    def tag_cloud_size(self):
        return self.total_uses * 0.75

    @property
    def total_uses(self):
        return self.chroniko_taggedentry_items.count()

    def get_absolute_url(self):
        return reverse("blog:tag_detail", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.pk:
            self.name = self.name.strip(",")
        super().save(*args, **kwargs)


class TaggedEntry(GenericTaggedItemBase):
    tag = models.ForeignKey(
        BlogTag, related_name="%(app_label)s_%(class)s_items", on_delete=models.PROTECT
    )

    class Meta:
        db_table = "tagged_blog_entries"
