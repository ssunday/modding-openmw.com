# Generated by Django 3.2.10 on 2024-02-24 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('momw', '0035_auto_20231223_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='datapath',
            name='for_cfg',
            field=models.BooleanField(default=True),
        ),
    ]
