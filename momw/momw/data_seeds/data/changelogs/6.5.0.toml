# {% load momwtags %}

##################################################
#
# Changelog TOML schema:
#
# [[log_entry]]
# text = """{% mod_link 'V.I.P. - Vegetto\'s Important Patches' %}"""
# date = "YYYY-MM-DD"
# modlists = [
#   "mod-list-slugs",
#   "go-here",
#   "like-this",
# ]
# kind = "added" # or "removed" or "updated"
#
# The "text" field can make full use of all django template tags and filters:
#
# https://docs.djangoproject.com/en/4.2/ref/templates/builtins/#ref-templates-builtins-tags
# https://docs.djangoproject.com/en/4.2/ref/templates/builtins/#ref-templates-builtins-filters
#
# Our project provides three custom tags:
#
# mod_link renders a link to the given mod's page
# moved_mod_link renders a link for a mod that's not in the DB, redirects to one that is
# issue_link renders a link to a GitLab issue with the given number
#
# **Please note** that mod names with a single quote in
# them must be escaped, see the example above.
#
##################################################

[[log_entry]]
text = """{% mod_link 'HD Forge for OpenMW-Lua' %} as a dev build only option."""
date = "2024-03-13"
modlists = [
  "total-overhaul",
  "graphics-overhaul"
]
kind = "added"

[[log_entry]]
text = """{% mod_link 'OpenMW vanilla candles patched with Enlightened Flames and ILFAS' %}"""
date = "2024-03-13"
modlists = [
  "total-overhaul",
  "expanded-vanilla",
  "graphics-overhaul"
]
kind = "added"

[[log_entry]]
text = """{% mod_link 'Project Atlas' %} now recommends to use the <code>01 Textures - MET</code> folder, since omitting it resulted in missing textures for a few items."""
date = "2024-03-11"
modlists = [
  "total-overhaul",
  "graphics-overhaul",
  "one-day-morrowind-modernization"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'MOMW Patches' %} added a plugin to delete negative light records for OpenMW 0.48."""
date = "2024-03-11"
modlists = [
  "total-overhaul",
  "expanded-vanilla",
  "graphics-overhaul",
  "one-day-morrowind-modernization"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Sword and ring of nerevar' %} now recommends to download the Moon-and-Star assets as well."""
date = "2024-03-11"
modlists = [
  "total-overhaul",
  "graphics-overhaul"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Oh No Stolen Reports' %} no longer has a separate plugin for BCOM compatibility, the base plugin supports it and Vanilla."""
date = "2024-03-10"
modlists = [
  "total-overhaul",
  "expanded-vanilla",
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Expansion Resource Conflicts' %}, since the issue that this mod fixes is already fixed by other mods in this modlist."""
date = "2024-03-09"
modlists = [
  "expanded-vanilla",
  "i-heart-vanilla",
  "i-heart-vanilla-directors-cut"
]
kind = "removed"

[[log_entry]]
text = """{% mod_link 'Know Thy Ancestors' %} usage notes updated (the {% mod_link 'Tamriel Rebuilt' %} addon is no longer recommended due to incompatibility with the latest version of the mod)."""
date = "2024-03-09"
modlists = [
  "total-overhaul",
  "graphics-overhaul",
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Unofficial Morrowind Official Plugins Patched' %} usage notes and data paths updated for the newest version of the mod."""
date = "2024-03-09"
modlists = [
  "one-day-morrowind-modernization",
  "i-heart-vanilla",
  "i-heart-vanilla-directors-cut"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Starwind Community Patch Project' %} download link, data path and plugins updated for the newest version of the mod."""
date = "2024-03-09"
modlists = [
  "starwind-modded"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Apothecary\'s Demise' %} due to some incompatibilities with OpenMW."""
date = "2024-03-07"
modlists = [
  "expanded-vanilla"
]
kind = "removed"

[[log_entry]]
text = """{% mod_link 'Apothecary\'s Demise' %} and its associated normal maps from {% mod_link 'Normal Maps for Everything' %} due to some incompatibilities with OpenMW."""
date = "2024-03-07"
modlists = [
  "total-overhaul"
]
kind = "removed"

[[log_entry]]
text = """All dev build only mods (and dev build only features for mods that are themselves not) are now opt-in for all mod lists."""
date = "2024-03-06"
modlists = [
  "total-overhaul",
  "graphics-overhaul",
  "expanded-vanilla",
  "one-day-morrowind-modernization",
  "just-good-morrowind"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Maar Gan - Town of Pilgrimage' %} now includes a fix for the Redoran hut meshes from {% mod_link 'OAAB_Data' %}."""
date = "2024-03-06"
modlists = [
  "total-overhaul",
  "graphics-overhaul",
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'MBSP Uncapped (OpenMW Lua) - NCGD Compat' %} is now included with {% mod_link 'NCGDMW Lua Edition' %}"""
date = "2024-03-05"
modlists = [
  "total-overhaul",
  "one-day-morrowind-modernization",
  "expanded-vanilla",
  "just-good-morrowind"
]
kind = "removed"

[[log_entry]]
text = """{% mod_link 'Interesting Outfits - Solstheim' %}"""
date = "2024-03-05"
modlists = [
  "total-overhaul",
  "graphics-overhaul",
  "expanded-vanilla"
]
kind = "added"

[[log_entry]]
text = """{% mod_link 'Yet Another Guard Diversity Expanded Imperials' %} and {% mod_link 'Boots for Beasts - Amenophis Revision' %} don't recommend the Bloodmoon options anymore due to limited compatibility with {% mod_link 'Solstheim Tomb of the Snow Prince' %}."""
date = "2024-03-05"
modlists = [
  "total-overhaul",
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Yet Another Guard Diversity Expanded Imperials' %} now doesn't recommend the Bloodmoon option anymore due to limited compatibility with {% mod_link 'Solstheim Tomb of the Snow Prince' %}."""
date = "2024-03-05"
modlists = [
  "graphics-overhaul"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Subtle Animated Menu Background Assemble' %} has an updated data path."""
date = "2024-03-05"
modlists = [
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Groundcoverify' %} as the issues with the large modlists have been fixed in the latest version of {% mod_link 'Delta Plugin' %}."""
date = "2024-03-03"
modlists = [
  "total-overhaul",
  "expanded-vanilla"
]
kind = "added"

[[log_entry]]
text = """{% mod_link 'Vurts Solstheim Tree Replacer II' %} due to incompatibility with {% mod_link 'Solstheim Tomb of the Snow Prince' %}."""
date = "2024-03-02"
modlists = [
  "total-overhaul",
  "graphics-overhaul"
]
kind = "removed"

[[log_entry]]
text = """{% mod_link 'NCGDMW Lua Edition' %} has simplified usage instructions, you can now just let the CFG Generator give you the correct options for your mod list and whether or not you've opted into dev build features <a href=\"{% url 'user_settings' %}\">on the user settings page</a>."""
date = "2024-02-24"
modlists = [
  "total-overhaul",
  "expanded-vanilla",
  "one-day-morrowind-modernization",
  "starwind-modded",
  "just-good-morrowind"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Religions Elaborated' %} due to incomplete or misleading dialogues."""
date = "2024-02-24"
modlists = [
  "total-overhaul",
  "expanded-vanilla"
]
kind = "removed"

[[log_entry]]
text = """{% mod_link 'Mines and Caverns' %} plugins, data paths, and usage notes updated for the new version of the mod. The replacer plugin from {% mod_link 'MOMW Patches' %} is no longer required, and the patch collection now offers a new patch plugin that resolves some compatibility issues as patch no. 14."""
date = "2024-02-24"
modlists = [
  "total-overhaul",
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Mines and Caverns' %} plugins, data paths, and usage notes updated for the new version of the mod. The replacer plugin from {% mod_link 'MOMW Patches' %} is no longer required."""
date = "2024-02-24"
modlists = [
  "graphics-overhaul"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'MOMW Patches' %} added a new compatibility patch for {% mod_link 'Daedric Shrine Overhaul Sheogorath' %} and {% mod_link 'Blademeister' %}."""
date = "2024-02-24"
modlists = [
  "total-overhaul",
  "expanded-vanilla"
]
kind = "updated"

[[log_entry]]
text = """{% mod_link 'Graphic Herbalism - MWSE and OpenMW Edition' %} swapped the <code>correctUV Ore Replacer_respawning.esp</code> plugin for <code>correctUV Ore Replacer_fixed.esp</code> for balance reasons and to mimic vanilla behavior."""
date = "2024-02-20"
modlists = [
    "total-overhaul",
    "graphics-overhaul",
    "expanded-vanilla",
    "one-day-morrowind-modernization",
    "i-heart-vanilla",
    "i-heart-vanilla-directors-cut",
    "just-good-morrowind"
]
kind = "updated"
