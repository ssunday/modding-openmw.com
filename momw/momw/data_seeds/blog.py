from datetime import datetime
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify
from chroniko.models import BlogCategory, BlogEntry, BlogTag, TaggedEntry

User = get_user_model()


def generate_category(title, slug, desc):
    c = BlogCategory.objects.create(title=title, slug=slug, description=desc)
    c.save()
    return c


def generate_entry(
    body, date_added, status, title, author, category, excerpt="", slug=None
):
    """
    {
        'body': rikeripsum.generate_paragraph(100),
        'date_added': datetime.strptime('2010-12-18 14:34:34 -0500', datetime_format),
        'slug': 'blog-entry-one',
        'status': 1,
        'title': 'Blog Entry One LIVE',
        'author': u,
        'category': cat1
    }
    """
    if slug:
        e = BlogEntry.objects.create(
            body=body,
            date_added=date_added,
            slug=slug,
            status=status,
            title=title,
            author=author,
            category=category,
            excerpt=excerpt,
        )
    else:
        e = BlogEntry.objects.create(
            body=body,
            date_added=date_added,
            slug=slugify(title),
            status=status,
            title=title,
            author=author,
            category=category,
            excerpt=excerpt,
        )
    e.save()
    return e


def blog_category_factory():
    BlogCategory(
        title="Announcements",
        slug="announcements",
        description="Announcements, messages, things you want to read.",
    ).save()
    BlogCategory(
        title="Updates", slug="updates", description="News about updates."
    ).save()


def blog_tag_factory():
    BlogTag(name="HelloWorld").save()


def blog_entry_factory():
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    entry_content_type = ContentType.objects.get_for_model(BlogEntry)
    u = User.objects.get(pk=1)
    announcements = BlogCategory.objects.get(slug="announcements")
    updates = BlogCategory.objects.get(slug="updates")
    tag1 = BlogTag.objects.get(pk=1)

    generate_entry(
        **{
            "body": """<p>Outlander,</p>

<p>It's with great pleasure that today I announce the release of the source code for this very website.  While this is actually quite a late announcement (I shared the news on <a href="https://forum.openmw.org/viewtopic.php?f=4&t=5901">the OpenMW forums</a> a few weeks back), I wanted to make sure to make a post about it here.  Many thanks to all who use the site, and to the amazing community of modders that create the content which keeps the game we all love alive and growing.</p>

<p>All issues are now publicly viewable on <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw/issues">this page</a>, and are grouped by milestone <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw/milestones">here</a> (for those interested in a rough timeframe for when a change might happen).  For those interested in hacking on the code, the source repo can be found <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw">here</a>, please take a look at <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw/src/commit/29efd9370718ad40677007b01ee060b357d0a4d1/CONTRIBUTING.md">the contributing guide</a> too.  For those of you that are Source Hut fans, I've got <a href="https://git.sr.ht/~hristoast/modding-openmw.com">a mirror on there as well</a>.</p>

<p>An account is required to directly submit issues, but as always I will gladly submit an issue on your behalf if you reach out via email.  Additionally, I am happy to accept code patches via email (see <a href="https://git-send-email.io/">here</a> for more details).</p>

<p>I think that's it.  As a result of the issues and code repo being public I see less of a need for posts here, but when there's something worth noting it shall be noted here!  Thanks again, and catch you all later.</p>

<p>-Hristos (a.k.a. M'aiq The Admin)</p>
""",
            "date_added": datetime.strptime(
                "2019-05-08 18:12:55 -0500", datetime_format
            ),
            "status": 1,
            "title": "Free Software Edition",
            "author": u,
            "category": announcements,
        }
    )

    generate_entry(
        **{
            "body": """<p>Hello, Outlander-</p>

<p>Anyone who has used the search on this site may have noticed the ... odd results it could sometimes return (not to mention other quirks.)  Today I am pleased to announce a vastly improved search backend that actually tries to properly rank and sort results based on your search term.  Thanks to everyone who searched the site before and put up with shoddy results -- those days are over!</p>

<p>Consider that previously, a search for "patch" would land <a href="/mods/patch-for-purists/">Patch For Purists</a> somewhere in the middle of the results list.  How odd, right?  Now, <a href="/search/?q=patch">it's found at the top of the list of results as one might expect</a>!</p>

<p>Thanks for visiting! -M'aiq</p>
""",
            "date_added": datetime.strptime(
                "2019-04-07 14:11:25 -0500", datetime_format
            ),
            "status": 1,
            "title": "Better Searching Is Here",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p><span class="bold">UPDATED 2019-03-05</span> Thanks to the awesome work of the OpenMW team - users of OpenMW nighly builds going forward can now see all kinds of beautiful shadows.  This exciting news has brought your friendly site admin out of hybernation to bring you some updates to the site.  In no particular order:</p>

<ul>
  <li>Added the "needs cleaning" field as well as <a href="/tips/cleaning-with-tes3cmd/">the tips page on cleaning plugins</a>.  Big thanks to Enkida, whose knowledge, research, and modding wisdom was an immense help in building this into the site.</li>
  <li>I've added a huge amount of mods, greatly expanding the catalog of documented mods.  Normally I'd list each one I added here but it's gotten to the point that so many are added, it would kind of be a pain to make folks find that information here.  To that end I created the "<a href="/mods/latest/">the latest mods page</a>", which lists mods in the order that they've been added to the site.  For now just 100 are shown, but eventually I'll paginate the list and show all mods there.</li>
  <li>Since shadows are now released, I've added a section about them to <a href="/getting-started/settings/">the tweak settings page</a>.</li>
  <li>The <a href="/cfg/">CFG Generator</a> has been revamped in several ways:
    <ul>
     <li>All mods cataloged on the site are selectable and have appropriate data path and plugin ordering.</li>
     <li>Data paths are now provided.</li>
     <li>Any extra configs required by any selected mods are now provided.</li>
     <li>Conflicts not yet handled, so keep that in mind when using the generator.</li>
    </ul>
  </li>
  <li>Added a cool "find in files" tip for Windows to <a href="/tips/shiny-meshes/">the Shiny Meshes page</a> -- big thanks to Luke for that!</li>
  <li>31 new screen shots have been added, <a href="/media/images/">go check them out</a>!</li>
  <li>Updated descriptions and attributes of many mods that were already listed.  Most changes were corrections, but others included adding "needs cleaning" values and etc.</li>
  <li>It's worth mentioning here that UC Nature has been pulled from the Total Overhaul List.  At least on my primary rig, testing that mod on its own showed massive FPS drops in certain areas like the Ascadian Isles just outside of Pelagiad.  This mod added a ton of content, so it's no surprise that it might affect performance, and of course your mileage may vary.</li>
  <li>Fixed the data ordering of several mods to avoid conflicts.  Of note: Dwemer textres and meshes are now loaded after <a href="/mods/sobitur-facility/">Sobitur Facility</a>.</li>
  <li>And a bunch of other changes that nobody would really care about.  In all, I closed over 100 issues on the site's issue tracker and made over 200 commits in the site git repository.</li>
</ul>

<p>As always, thanks for visiting! -M'aiq</p>
""",
            "date_added": datetime.strptime(
                "2019-02-23 21:00:00 -0500", datetime_format
            ),
            "status": 1,
            "title": "An admin sees their shadow...",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Outlander-</p>
<p>Your friendly neighborhood site admin here again with another update that brings some critical new content:</p>

<ul>
  <li>Huge thanks to Enkida, who has offered massive amounts of input on cleaning plugins with the now-listed <a href="/mods/tes3cmd/">tes3cmd</a>.</li>
  <li>The syntax of <a href="/tips/file-renames/#windows">the powershell one-liner I offered for renaming files</a> was slightly off.  Many thanks to those that reported the problem to me, sorry about any trouble it may have caused!</li>
  <li>Recommended folder paths are no longer shown for incompatible mods, hopefully reducing potential confusion about their usage.</li>
  <li>Updated the plugin name for <a href="/mods/province-cyrodiil/">Province Cyrodiil</a>.</li>
  <li>An anonymous reader wrote in that the <a href="/mods/better-sounds-11-patch2/">Better Sounds 1.1 Patch 2</a> plugin had a script error, I've published a fixed version of that which is now linked to from that mod's page.</li>
</ul>

<p>That's all for now, I'm headed to Solstheim where I can write some code in the snow...</p>
""",
            "date_added": datetime.strptime(
                "2018-12-02 19:00:00 -0500", datetime_format
            ),
            "status": 1,
            "title": "It Doesn't Snow On Vvardenfell",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Hello, Outlanders-</p>
<p>I am squeezing one more update in, it started as a small update and then grew in size.  The details are below:</p>
<ul>
  <li>Added <a href="/mods/regen-and-stamina-overhaul/">Regen and Stamina Overhaul</a> by Smokeyninja -- this is a nice alternative to <a href="/mods/better-regen/">Better Regen</a> and other regen/stamina mods, and it was made specifically for OpenMW!</li>
  <li>Many thanks to Enkida, who sent in several helpful comments -- one of which was the fact that <a href="/mods/clear-your-name/">Clear Your Name</a> was already documented as not working with OpenMW.  I've edited the mod's status and removed it from the various lists it was on for now.</li>
  <li>And many thanks to Derek as well, who also submitted several helpful comments including the fact that the CFG Generator would print duplicate <code>content=Windows Glow - Tribunal Eng.esp</code> entries.  This is now fixed.</li>
  <li>Removed the warnings from <a href="/mods/vivec-guild-of-mages-expansion/">Vivec Guild of Mages Expansion</a> and <a href="/mods/rise-of-house-telvanni/">Rise of House Telvanni</a> about a potential conflict.  Thanks to Kevin for testing this and reporting back!</li>
  <li>Over the past several months I've gotten several emails about the Fliggerty-hosted download link for <a href="/mods/unification-compilation-armory/">Unification Compilation: Armory</a> going down.  After giving it much thought, I've decided to share a download link for the unmodified collection.  I will eventually share a version with de-shiny'd meshes, once I've ensured my copy has been fully and properly edited.</li>
  <li>I've refactored how I represent plugin and BSA ordering, which will make it much easier for me to add mods with plugins and alter the recommended load order.  This change is mostly to make working on and updating this site easier, but you the user will benefit since it means less friction when adding mods and changing the load order.</li>
  <li>I've tweaked the banner on the beta site to show the git rev of the code that's running.  I wanted to give folks a way to see what changes are in the beta version versus the "released" one, this is one step towards that.</li>
</ul>
<p>As always, a huge thank you to anyone I forgot to mention!  The number of folks that have reached out and contributed fixes and/or tips is so huge, it stuns me and I'm thankful for such an amazing community.  This website would just be a silly hobby without you.</p>

<p>There's plenty more coming; over 50 open issues remain on my internal issue tracker!  I hope to have many more changes out before the year's end, including the FOSS-ing of this site's code.  Until then.....</p>
""",
            "date_added": datetime.strptime(
                "2018-11-30 19:00:00 -0500", datetime_format
            ),
            "status": 1,
            "title": "November Update #2",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Howdy, outlander-</p>

<p>Today I bring you a small update with some key fixes, it's the first in a series of updates I've got planned.  For now, look forward to these changes:</p>

<ul>
    <li>Fixed a problem with the feedback form when submitting a new mod.</li>
    <li>Updated <a href="/mods/patch-for-purists/">Patch For Purists</a> to indicate that the Official Add-ons patch is no longer provided.  An alternative is recommended by the Patch For Purists author, I will be reviewing it for inclusion on this site soon..</li>
    <li>Added <code>Misc_LW_Platter.nif</code> to the list of what should be deleted on <a href="/mods/mesh-improvements-optimized/">Mesh Improvements Optimized</a>.</li>
</ul>

<p>As always, many thanks to those of you who emailed me about changes and anything else.  Look forward to more fixes and additions, but until then...</p>

<p>M'aiq</p>""",
            "date_added": datetime.strptime(
                "2018-11-25 18:40:00 -0500", datetime_format
            ),
            "status": 1,
            "title": "November Update",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Greetings- today I bring you a small update featuring corrections and fixes.  These are things you, the reader have submitted and called out.  Many thanks to all of you!</p>
<ul>
  <li><a href="/mods/magicka-based-skill-progression-ncgdmw-compatility/">Magicka Based Skill Progression - ncgdMW Compatility Version</a> needs to load after <a href="/mods/natural-character-growth-and-decay-mw/">Natural Character Growth and Decay - MW</a>, per the instructions on the download page.  Somehow I missed that, or mixed it up!  Anyways, <a href="/cfg-generator/">the CFG Generator</a> has been corrected and now recommends the right load order.  Thanks P for pointing that out!</li>
  <li>Added a note to the <a href="/errata/">Errata</a> about mods that have shiny meshes but no file list to give users a clue about what needs editing.  Someday this will be more complete, for now: please accept my apologies about that.</li>
  <li>Added notes to <a href="/mods/vivec-guild-of-mages-expansion/">Vivec Guild of Mages Expansion</a> and <a href="/mods/rise-of-house-telvanni/">Rise of House Telvanni</a> indicating that they may conflict.  I'm working on verifying this and will update the site accordingly as soon as I know either way.</li>
  <li>Added a note to the <a href="/tips/tr-patcher/">TR Patcher tips page</a> indicating that the patcher does not seem to work with Java 10.  Beware!</li>
  <li>Kuyondo pointed out that the download link for <a href="/mods/city-of-balmora-hlaalu-expansion/">City of Balmora - Hlaalu Expansion</a> was wrong (a totally different mod) -- oops!  That's been corrected.</li>
  <li>Improved parts of <a href="/cfg-generator/">the CFG Generator</a> code by de-hardcoding some things.</li>
  <li>Shuffled the load order of <a href="/mods/graphic-herbalism-add_on-no-glow/">Graphic Herbalism ADD_ON (No Glow)</a> so that it loads after <a href="/mods/graphic-herbalism/">Graphic Herbalism</a>, per the docs.  Thanks again P for this one.</li>
</ul>
<p>Until next time..  M'aiq</p>""",
            "date_added": datetime.strptime(
                "2018-08-10 21:00:00 -0500", datetime_format
            ),
            "slug": "a-small-bugfix-corrections-update",
            "status": 1,
            "title": "A small bugfix/corrections update",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Outlander, today drops another decent-sized update.  Below are the details of what all has changed or been added:</p>
<ul>
  <li>Added the <a href="/cfg-generator/">CFG Generator</a>, which will give you the right plugin load order based on the mods you select.  Also expanded on how I determine the given load order on the "<a href="/load-order/">Load Order</a>" page.</li>
    <ul>
      <li>Note that this does not currently warn you about or solve conflicts (save for one case, as a test), but that will be coming in the near future.</li>
      <li>Additionally, the data paths part of the cfg file is not produced, nor are any other parts of the file (such as weather values, UI, etc.) -- but this will be added in the near future.  For now a list of used mods is provided, and their ordering is what the data path ordering should be.</li>
    </ul>
  <li>Added <a href="/feedback/">an enhanced user feedback form</a> - this was inspired by comments from folks on the OpenMW forums who expressed a desire for a better way for users to submit information about a given mod.  My hope is that this makes it easier for anyone to request a mod addition or update if they so desire.</li>
  <li>Removed <a href="/mods/improved-telvanni-architecture/">Improved Telvanni Architecture</a> from the recommended list.  It had performance issues in larger cities, so I decided against using it.</li>
  <li>Added <a href="#TODO">Quill of Feyfolken - Scroll Enchanting</a>, which adds enchanted scroll making.  Shout-out to <a href="https://forum.openmw.org/viewtopic.php?f=40&t=5321">kuyondo</a> on the OpenMW forums for finding and recommending this one.</li>
  <li>Added <a href="/mods/widescreen-alaisiagae-splash-screens/">Widescreen Alaisiagae Splash Screens</a>, I've used it for a while but just forgot to have it featured.</li>
  <li>Added <a href="/mods/welcome-to-the-arena/">Welcome to the Arena</a> and the required <a hred="/mods/welcome-to-the-arena-openmw-patch/">patched plugin for OpenMW</a>.  Many thanks to Luke for the great suggestion!</li>
  <li>Added <a href="/mods/chargen-revamped-expanded-lands/">Chargen Revamped - Expanded Lands</a> as an alternative to <a href="/mods/better-chargen/">Better Chargen</a>.  Another shout-out to Luke for this!</li>
  <li>Added <a href="/mods/city-of-balmora-hlaalu-expansion/">City of Balmora - Hlaalu Expansion</a> as an alternative to <a href="/mods/kilcundas-balmora/">Kilcunda's Balmora</a> -- this one seems like a must have for those of you from House Hlaalu!  Thanks again to Luke for reminding me that this one was on the TODO list and looked awesome.</li>
  <li>Added <a href="/mods/mines-and-caverns/">Mines and Caverns</a>.  I've known about this one for a while but was hesitant to check it out for whatever reason.  Props again to Luke for encouraging me to go for it!</li>
  <li>Added <a href="/mods/wealth-within-measure/">Wealth Within Measure</a> and also corrected links and naming on <a href="/mods/the-cornerclub-scripts-tes3mp/">The Cornerclub Scripts (TES3MP)</a>.  Thanks a bunch to Texafornian for the recommendation and also for advising me on stuff that needed changing.</li>
  <li>Added a mention of the compatibility plugin for <a href="/mods/uviriths-legacy-35/">Uvirith's Legacy 3.5</a> and <a href="/mods/rise-of-house-telvanni/">Rise of House Telvanni</a>.  This is another one I knew about and had, but had simply forgotten about in some mod shuffle.  Thanks Luke for the reminder about this one!</li>
  <li>Updated <a href="/mods/uviriths-legacy-35/">Uvirith's Legacy 3.5</a> to mention that the included TR plugin is outdated, and added <a href="/mods/uviriths-legacy-35-tr-add-on-1807/">this</a> which to be used instead.  Thank you Luke for the heads up on this.</li>
  <li>Added the incompatible mod, <a href="/mods/controlled-consumption/">Controlled Consumption</a>.  Thanks to Alex for requesting this, unfortunately the dependency on MWSE means it won't work for OpenMW - I've indicated this on the mod's detail page with a circle-slash icon and some hover text (like this: <i class="fa fa-ban" title="This mod is currently NOT compatible with the latest OpenMW!"></i>), this will be present on any future incompatible mods that are added.</li>
  <li>Added notes to <a href="/mods/septim-gold-and-dwemer-dumacs/">Septim Gold and Dwemer Dumacs</a> that clarify the need to only edit one set of meshes for either it or <a href="/mods/realistically-stacked-septims-and-gold-bags/">the realistically stacked variant</a>.  Another big thanks goes out to Luke for reminding me about this, <a href="https://www.youtube.com/watch?v=X7dFMbubxr4&t=7">he's on fire</a> with all the QA help!</li>
  <li>Added <a href="/tips/openmw-physics-fps/">a tips page about setting the <code>OPENMW_PHYSICS_FPS</code> environment variable</a>, which may add noticeable performance gains.</li>
  <li>Added more TES3MP-specific content.  Check out <a href="/mods/category/tes3mp/">the category</a> or <a href="/mods/tag/tes3mp-friendly/">the tag</a>!  Also make sure to check out the <a href="/mods/ncgd-alt-start-patched-for-tes3mp/">TES3MP patch for NCGD</a> that I put together.</li>
  <li>Added a link to <a href="/tes3mp/">the Modding-OpenMW.com TES3MP server page</a> -- modded multiplayer in Vvardenfell hosted by yours truly!  Please bear in mind that the server is currently in open alpha, with only Linux clients available at the moment.  It's pretty stable but I'm still working on a few balance and gameplay scripts for the server.</li>
  <li>Updated the site <a href="/cookie/">cookie policy page</a> to explain how this site now uses them.</li>
  <li>Implemented buttons across the site.  Gone are the old walls of text links, now replaced with walls of buttons!</li>
  <li>Added the <a href="/tips/tr-patcher/">TR Patcher tips page</a>, which describes what the "TR Patcher" is, why you want to use it, and how to use it.</li>
  <li>Two new gameplay videos, <a href="/media/video/">Check them out</a>!</li>
  <li>Several <a href="/media/images/">new pictures</a>.  Check them out!</li>
  <li>Added a note to the <a href="/faq/">FAQ</a> about rehosting content.  This was prompted by an anonymous message I received asking me to do so.</li>
  <li>Each mod now has an "OpenMW Compatibility" rating, sort of an easy way to let you know how well a mod works.  Bear in mind, partially working might still mean "fully working", just with some manual intervention like renaming a file or something like that.  See <a href="/compatibility/">this page</a> for more information.</li>
  <li>The search bar now searches media (images, videos) and blog posts in addition to mods themselves.  This should make it easier to not only find a particular mod, but also related images, videos, or news about it.</li>
  <li>Unless you've blocked javascript, the search bar now gets focused on when you load any given page.  Maybe that's annoying in some way I'm not noticing, let me know if that's the case.</li>
  <li>Many other style changes and improvements throughout the site.</li>
  <li>A bunch of things that you the visitor will never notice (as usual, there's always a bunch of these kinds of changes.)</li>
</ul>

<p>This update has turned out to be even more massive than the last one, which I thought was pretty huge.  Over 25 issues closed in the site issue tracker, and more than 135 VCS commits to the site's codebase.  My CI job count passed 400 yesterday!</p>

<p>Many thanks to all the folks who contacted me about recommendations, or just to chat.  You all rock!  Also: another very huge thanks to Ravenwing, Thunderforge, kuyondo, psi29a, and all other folks from the OpenMW forums for their suggestions and support!</p>

<p>Update: Added yet another gameplay video, for a total of two new ones!</p>""",
            "date_added": datetime.strptime(
                "2018-08-05 21:00:00 -0500", datetime_format
            ),
            "slug": "update-time-play-openmw",
            "status": 1,
            "title": "Update time, go play some modded OpenMW",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Greetings, visitor.  Another very small update today, please be sure to check out the new usage notes for <a href="/mods/better-flora/">Better Flora</a> to ensure your trees look as great as they can (there's a mesh conflict with <a href="/mods/unification-compilation-nature/">UC: Nature</a>.)</p>""",
            "date_added": datetime.strptime(
                "2018-07-06 21:00:00 -0500", datetime_format
            ),
            "slug": "another-update-for-the-trees",
            "status": 1,
            "title": "Another update: for the trees",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Greetings, outlander - although a major update just dropped, it's time once again for a minor update.  This includes a few fixes to the recommended list as well as some other changes:</p>

<ul>
    <li>Clarified that you only want to download the quill replacer for <a href="/mods/r-zeros-random-retextures-and-replacers/">R-Zero's Random Retextures (and Replacers)</a>.</li>
    <li>Added <a href="/mods/vurts-groundcover-for-openmw/">Vurts Groundcover for OpenMW</a> to the recommended list.  Note there are some conflicts, see the usage notes for more details.</li>
    <li>Removed <a href="/mods/correct-uv-trees/">Correct UV Trees</a> from the recommended list as all of its files were superceded by <a href="/mods/patch-for-purists/">Patch For Purists</a>.</li>
    <li>Removed <a href="/mods/pluginless-noglow/">Pluginless NoGlow</a> from the recommended list as its files were superceded by <a href="/mods/unification-compilation-armory/">Unification Compilation: Armory</a>.</li>
    <li>Adjusted tags on several mods, the overall goal being to ensure that the "Expanded Vanilla" tag doesn't have any mods that include high definition meshes or textures.  Keep it vanilla, or something close to that.</li>
    <li>Fixed formatting of the description and/or the usage notes on several mods.  Things should be more readable in general.</li>
    <li>Added a few more gameplay screenshots, these ones feature variants of the recommended list with different texture packs.</li>
    <li>Made the various media links more prominent, also added a note about this blog at the top of the recommended list.  My hope is folks will notice that link and find the various changelogs in here.</li>
    <li>Updated the note about mannequins in the errata.</li>
    <li>Added the <a href="/mods/openmw-modchecker/">OpenMW-ModChecker</a> tool - this command-line utility allows one to scan their OpenMW load order to determine if any mods are totally overridden by later entries.  I've used this tool on the recommended list to weed out a few mods that are actually completely overridden.</li>
</ul>

<p>Unless any major issues are found, the next update won't be for a while -- but don't worry, it's set to contain a lot of great things (I currently have 22 open issues in my private bug tracker for the site)!</p>
        """,
            "date_added": datetime.strptime(
                "2018-07-04 21:00:00 -0500", datetime_format
            ),
            "slug": "update-time-once-again",
            "status": 1,
            "title": "Update time once again",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Welcome back, outlander.  On this fine day I've decided that this site is no longer considered a "beta".  Months of work and countless hours of playtesting have brought me here, and I'd like to list just a few of the more noteable changes that I've rolled out:</p>

        <ul>
          <li>Added <a href="/mods/high-poly-candles-fixed/">High Poly Candles</a>, <a href="/mods/divine-dagoths/">Divine Dagoths</a>, and <a href="/mods/morrowind-optimization-patch/">Morrowind Optimization Patch</a> to the recommended list.</li>
          <li>Removed <a href="/mods/divine-dagoth-ur/">Divine Dagoth Ur</a> and <a href="/mods/talrivians-state-based-hp-mod/">Talrivian's State-Based HP Mod</a> from the recommended list, but they are still featured as alternatives.  The former has been superceded by "<a href="/mods/divine-dagoths/">Divine Dagoths</a>", and the latter was not compatible with <a href="/mods/natural-character-growth-and-decay-mw/">NCGDMW</a>.</li>
          <li>Updated <a href="/mods/apels-lighthouse-retexture/">Apel's Lighthouse Retexture</a>, there  was an OpenMW-specific version that I missed.</li>
          <li>Updated the description and/or usage notes on a large portion of mods.</li>
          <li>Added Mod "<a href="/mods/tag/">tags</a>" as another way to group mods.  For instance, those looking for an expanded vanilla experience minus the HD graphical changes could only use mods tagged "<a href="/mods/tag/expanded-vanilla/">Expanded Vanilla</a>".  I'd love feedback about this.</li>
          <li>Gameplay <a href="/media/images/">images</a> and <a href="/media/video/">videos</a> are now featured.  There's only one of each at the moment, but I plan to expand this as I log more time in the game.</li>
          <li>Expanded the "<a href="/getting-started/">Getting Started</a>" section, it now has its own sub-pages.  More content than I can talk about in one line.</li>
          <li>Expanded the "<a href='/tips/'>Tips</a>" section as well;  it too now has sub-pages that go into more detail on various key topics.</li>
          <li>Added a "<a href='/mods/todo/'>TODO list</a>" page, where I list out mods I'd like to evaluate for addition to the site.</li>
          <li>Link to the "<a href='/errata/'>Errata</a>" page from the front page.  This page existed for a while but was pretty much empty.</li>
          <li>Fixed several formatting issues on both mobile and desktop views.</li>
          <li>Many many under-the-hood tweaks that you will never notice, but make running/growing the site easier for me.</li>
          <li>Much more that's not listed here.  In total over 100 commits to the site's VCS repo!</li>
        </ul>

        <p>But that's not all, here's what I have planned for the future (in no particular order):</p>

        <ul>
          <li>An <code>openmw.cfg</code> generator -- using this website pick and choose the mods you want and generate a downloadable, usable config file.</li>
          <li>An advanced search page that allows for more granular grouping and filtering.</li>
          <li>More image and video samples of various mods.</li>
          <li>More TES3MP stuff in general.  Someday there will be an official "Modding-OpenMW.com" server!</li>
          <li>If I can get the TR folks to share their code, I'd love to re-host their excellent name generator.  Failing that I might look into writing my own.</li>
          <li>I've written a few small, convenience mods that I'd like to share at some point.  They do things like autoselect MCA and NCGD values so you don't have to see any menus when you start a new game.  Others are patches for mods that need manual edits (meshes, texture renames, etc.)</li>
          <li>Whatever else I can't think of right now, or haven't yet thought of.</li>
        </ul>

        <p>As always, huge thanks to all the folks who submitted feedback.  A round of skooma on me next time you're in my neck of the webs...</p>""",
            "date_added": datetime.strptime(
                "2018-06-30 21:00:00 -0500", datetime_format
            ),
            "slug": "out-of-beta",
            "status": 1,
            "title": "Out of Beta",
            "author": u,
            "category": announcements,
        }
    )

    generate_entry(
        **{
            "body": """<p>Greetings, outlander! Another fine update today, this time 69 mods have been added to the recommended list, as well as several other changes, fixes, and updates.</p>

        <p>Unfortunately, several mods in the recommended list do not work straight out of the box; many textures require renaming, meshes patching, and more. I do my best to indicate when this is needed in the 'usage notes', but in the future I'd like to publish detailed information on the various tweaks that need to be done to make things work.  Some things are already explained quite well in other places (e.g. the official OpenMW docs have a great article on how to 'de-shiny' mods with that particular problem.) but others aren't covered so well, so I'd like to fill that gap.</p>

        <p>In the meantime, enjoy the overhauled recommended mods list!  There's more to come on that front as well, of course.</p>""",
            "date_added": datetime.strptime(
                "2018-04-21 21:00:00 -0500", datetime_format
            ),
            "slug": "update-time",
            "status": 1,
            "title": "Update time!",
            "author": u,
            "category": updates,
        }
    )

    generate_entry(
        **{
            "body": """<p>Greetings and welcome to the first blog entry on Modding-OpenMW.com. This is your friendly neighborhood site admin saying hello, and be sure to drop a note using <a href="/contact/">our contact form</a>!</p><p>While the site is still in development or "beta" you won't see a lot of entries on this blog, but eventually this is where you'd find news about new mods, updates, and anything else worth announcing. The latest update to the site added many things: this blog, an array of new recommended mods, the start of a "<a href="/tips/">tips</a>" page, and updated navigation links on several pages (for starters.)</p>

        <p>So what's next for this site? Expect the recommended mod list to be expanded and updated to reflect my latest playtesting, as well as patches for shiny meshes that are available only here!  Also expect more guides on things like: renaming normal maps and other files so they work properly, how to pick and choose textures, and many more tips and tricks I've picked up and want to share.</p>

        <p>As for the more distant future, who knows what's in store.  Someday I'd love to even host mods and provide uploads for modders, if there's an interest. If you have an idea or request <a href="/contact/">I'd love to hear it</a>!</p>""",
            "date_added": datetime.strptime(
                "2018-04-13 21:00:00 -0500", datetime_format
            ),
            "slug": "hello-world",
            "status": 1,
            "title": "Hello, world!",
            "author": u,
            "category": announcements,
        }
    )
    entry1 = BlogEntry.objects.get(slug="hello-world")
    entry1.tags.add(tag1)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry1.id, tag=tag1
    ).save()
