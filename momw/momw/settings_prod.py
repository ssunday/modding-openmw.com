from .settings_server import *  # NOQA

MOMW_VER = False
PROJECT_HOSTNAME = "modding-openmw.com"
SITE_NAME = "Modding-OpenMW.com"
USE_ROBOTS = False
SERVER_EMAIL = f"app@{SITE_NAME}"
