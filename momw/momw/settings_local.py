ALLOWED_HOSTS = ["localhost", "127.0.0.1", "0.0.0.0"]
DEBUG = True
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025
MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]
PROJECT_HOSTNAME = "0.0.0.0"
SITE_NAME = "Local Modding-OpenMW.com"
USE_ROBOTS = True
CACHE_MIDDLEWARE_SECONDS = 1
SERVER_EMAIL = "app@TODO"
