import pytz

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import MediaTrack


TZ = pytz.timezone(settings.TIME_ZONE)
TITLE = "{}'s Latest Media".format(settings.SITE_NAME)
DESCRIPTION = TITLE


class LatestMediaFeed(Feed):
    description = DESCRIPTION
    link = "/media/"  # TODO: find out wtf this is for
    title = TITLE

    def items(self):
        return MediaTrack.live.all()

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return reverse("media:detail", kwargs={"slug": item.slug})
