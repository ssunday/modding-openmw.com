import pytz

from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from media.models import MediaTrack

TZ = pytz.timezone(settings.TIME_ZONE)
User = get_user_model()


def media_factory():
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    u = User.objects.get(pk=1)
    desc1 = """Using <a href="https://raw.githubusercontent.com/hristoast/dot-emacs/master/init.el">my Emacs configuration</a>.  C/C++ are the focus of this
clip, but you'll be set up for much more.  Downloading packages finishes at 1:00ish.  The following modes make this possible: <code>cc-mode</code>,
<code>company-irony</code>, <code>ggtags-mode</code>, <code>irony-mode</code>, and more!"""
    title1 = "Quick C/C++ IDE with Emacs and clang/LLVM"
    url1 = "/static/emacs-c-ide.json"
    slug1 = "emacs-c-ide"

    desc2 = """Small tool I wrote with Python 3.  Disable or enable runit services with style!"""
    title2 = "sv-disable / sv-enable / sv-mgr"
    url2 = "/static/sv-mgr-demo.json"
    slug2 = "sv-mgr"

    desc3 = """AUDIO AUDIO AUDIO AUDIO AUDIO"""
    title3 = """AUDIO AUDIO AUDIO AUDIO AUDIO"""
    url3 = "https://static.blackholegate.net/Infested.ogg"
    slug3 = "infested"

    desc4 = """VIDEO VIDEO VIDEO VIDEO VIDEO"""
    title4 = """VIDEO VIDEO VIDEO VIDEO VIDEO"""
    url4 = "https://static.blackholegate.net/A_Funny_Thing_Happened_on_the_way_to_the_Moon.mp4"
    slug4 = "a-funny-thing-happened-moon-landing-is-fake-sir"

    m1 = MediaTrack.objects.create(
        author=u,
        description=desc1,
        title=title1,
        url=url1,
        slug=slug1,
        media_type=MediaTrack.ASCIINEMA,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2011-02-22 16:34:34 -0500", datetime_format),
    )
    m2 = MediaTrack.objects.create(
        author=u,
        description=desc2,
        title=title2,
        url=url2,
        slug=slug2,
        media_type=MediaTrack.ASCIINEMA,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2011-01-19 10:34:34 -0500", datetime_format),
    )
    m3 = MediaTrack.objects.create(
        author=u,
        description=desc3,
        title=title3,
        url=url3,
        slug=slug3,
        media_type=MediaTrack.AUDIO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2010-12-18 14:34:34 -0500", datetime_format),
    )
    m4 = MediaTrack.objects.create(
        author=u,
        description=desc4,
        title=title4,
        url=url4,
        slug=slug4,
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2015-01-19 10:34:34 -0500", datetime_format),
    )

    m1.save()
    m2.save()
    m3.save()
    m4.save()
