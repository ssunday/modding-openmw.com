from django.contrib.auth.models import (
    AnonymousUser,
    User,
)  # TODO: get user function thing
from django.shortcuts import reverse
from django.contrib.sites.models import Site
from django.test import RequestFactory, TestCase
from .git import APP_SHA, GIT_TAG
from .views import app_info, app_info_json, contact


HTTP_OK = 200


# TODO: many if not all of the below are obsoleted by the above selenium tests
class UtilzTestCase(TestCase):
    def setUp(self):
        self.f = RequestFactory()
        self.s = Site.objects.create(name="test", domain="test.pizza")
        self.u = User.objects.create(
            first_name="Test", last_name="User", username="testuser", password="12345"
        )

    def test_contact_page(self):
        r = self.f.get(reverse("contact"))
        r.user = AnonymousUser()
        response = contact(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info(self):
        r = self.f.get(reverse("app-info"))
        r.user = AnonymousUser()
        response = app_info(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info_has_sha_or_tag(self):
        r = self.f.get(reverse("app-info"))
        r.user = AnonymousUser()
        response = app_info(r)
        try:
            self.assertContains(response, APP_SHA)
        except AssertionError:
            self.assertContains(response, GIT_TAG)

    def test_app_info_json(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info_json_has_sha_or_tag(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertTrue(
            APP_SHA in response.content.decode("utf-8")
            or GIT_TAG in response.content.decode("utf-8")
        )

    def test_app_info_json_is_json(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertTrue(
            "application/json; charset=utf-8" in response.headers["content-type"]
        )
