import json

from django.conf import settings
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render
from .forms import ContactForm
from .git import APP_SHA, GIT_TAG, is_tag_release


def app_info(request):
    return render(
        request,
        "app_info.html",
        {"sha": APP_SHA, "tag": GIT_TAG, "tag_release": is_tag_release()},
    )


def app_info_json(request):
    if is_tag_release():
        d = {"tag": GIT_TAG}
    else:
        d = {"sha": APP_SHA}
    data = json.dumps(d)
    return HttpResponse(data, content_type="application/json; charset=utf-8")


def contact(request):
    form = ContactForm()
    sent = False
    msg = request.GET.get("msg", None)
    if request.method == "POST":
        postdata = request.POST.copy()
        form = ContactForm(postdata)
        msg = postdata.get("msg", None)
        if form.is_valid():
            msg_name = postdata["name"]
            msg_email = postdata["email"]
            msg_message = postdata["message"]
            msg_ip = request.META["REMOTE_ADDR"]
            contact_msg = EmailMessage(
                "Contact Message from: {0} - {1} - {2}".format(
                    msg_ip, msg_name, msg_email
                ),
                msg_message,
                "{}".format(msg_email),
                ["contact@{}".format(settings.PROJECT_HOSTNAME)],
            )
            contact_msg.send()
            sent = True
    return render(request, "contact.html", {"form": form, "sent": sent, "msg": msg})
