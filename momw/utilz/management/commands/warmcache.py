from django.core.management.base import BaseCommand
from utilz.cache import warm_cache


class Command(BaseCommand):
    help = "warm the caches"

    def handle(self, *args, **options):
        warm_cache()
        self.stdout.write(self.style.SUCCESS("Successfully warmed the cache"))
